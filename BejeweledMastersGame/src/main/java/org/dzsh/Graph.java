package org.dzsh;

import org.javatuples.Quartet;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

public class Graph {
    public final static IntPredicate findMatch = new IntPredicate() {
        @Override
        public boolean test(int value) {
            return value >= 3;
        }
    };

    public final static IntPredicate findPotential = new IntPredicate() {
        @Override
        public boolean test(int value) {
            return value == 2;
        }
    };

    public final static IntPredicate findBonus = new IntPredicate() {
        @Override
        public boolean test(int value) {
            return value >= 4;
        }
    };

    private enum Color {
        WHITE, GRAY, BLACK
    }

    public Graph() {
    }

    public Quartet<HashMap<Gem, Gem>, HashMap<Gem, Integer>, HashMap<Gem, Integer>, AtomicInteger> DFS() {
        HashMap<Gem, Color> color = new HashMap<>();
        HashMap<Gem, Gem> pi = new HashMap<>();
        HashMap<Gem, Integer> d = new HashMap<>();
        HashMap<Gem, Integer> f = new HashMap<>();
        AtomicInteger time = new AtomicInteger();

        Consumer<Gem> DFS_visit = new Consumer<Gem>() {
            @Override
            public void accept(Gem u) {
                color.put(u, Color.GRAY);
                time.getAndIncrement();
                d.put(u, time.intValue());

                for (Gem vertex : u.adj()) {
                    if (color.get(vertex) == Color.WHITE) {
                        pi.put(vertex, u);
                        this.accept(vertex);
                    }
                }
                color.put(u, Color.BLACK);
                f.put(u, time.incrementAndGet());
            }
        };

        for (Gem u : Gem.getAllGems()) {
            color.put(u, Color.WHITE);
            pi.put(u, null);
        }

        for (Gem u : Gem.getAllGems()) {
            if (color.get(u) == Color.WHITE) {
                DFS_visit.accept(u);
            }
        }

        return new Quartet<>(pi, d, f, time);
    }

/*    private HashMap<Gem, LinkedList<Gem>> reversedPi(HashMap<Gem, Gem> pi) {
        HashMap<Gem, LinkedList<Gem>> revPi = new HashMap<>();

        for (Map.Entry<Gem, Gem> e : pi.entrySet()) {
            if (!revPi.containsKey(e.getValue())) revPi.put(e.getValue(), new LinkedList<>());
            revPi.get(e.getValue()).push(e.getKey());
        }
        return revPi;
    }
*/
/*
    private Pair<LinkedList<Gem>, HashMap<Gem, Integer>> sequenceGems(HashMap<Gem, LinkedList<Gem>> revPi) {
        LinkedList<Gem> gems = new LinkedList<>();
        HashMap<Gem, Integer> lengths = new HashMap<>();

        ObjIntConsumer<Gem> iterateTree = new ObjIntConsumer<Gem>() {
            @Override
            public void accept(Gem gem, int length) {
                int max = 0;

                if (revPi.containsKey(gem)) { // if not a leaf
                    for (Gem g : revPi.get(gem)) { // iterate all childs (adjacent gems)
                        this.accept(g, length + 1);
                        if (lengths.get(g) > max) max = lengths.get(g);
                    }
                    for (Gem g : revPi.get(gem)) {
                        lengths.put(gem, max);
                        if (lengths.get(g) >= 3) {
                            gems.push(gem);
                        }
                    }
                } else {
                    lengths.put(gem, length);
                }
            }
        };

        iterateTree.accept(null, 0);
        //
        for (Map.Entry<Gem, Integer> g : lengths.entrySet()) {
           if (g.getKey() != null && g.getValue() >= 3) System.out.println(g.getKey().getLocation().getValue0() + ", " + g.getKey().getLocation().getValue1() + " : " + g.getValue());
        }

        System.out.println("\nGemsToRemove:\n");

        for (Gem g : gems) {
            if (g != null) System.out.println(g.getLocation().getValue0() + ", " + g.getLocation().getValue1());
        }
        //
        return new Pair<LinkedList<Gem>, HashMap<Gem,Integer>>(gems.stream().filter((i) -> i != null).collect(Collectors.toCollection(LinkedList::new)), lengths);
    }
*/

    private LinkedList<LinkedList<Gem>> sequenceGems(HashMap<Gem, Gem> pi, IntPredicate f) {
        HashMap<Gem, LinkedList<Gem>> sequenceAncestorMapping = new HashMap<>();

        LinkedList<Gem> gemsToUpdate = new LinkedList<>();
        int length = 0;
        Gem gem = null, ancestor = null;

        for (Gem i : pi.keySet()) {
            gem = i;
            length = 0;

            do {
                gemsToUpdate.push(gem);
                if (pi.get(gem) == null) ancestor = gem;
                gem = pi.get(gem);
                length++;
            } while (gem != null);

            if (!(sequenceAncestorMapping.containsKey(ancestor)) || (length > sequenceAncestorMapping.get(ancestor).size())) { // (!(lengths.containsKey(ancestor)) || (length > lengths.get(ancestor)))
                LinkedList<Gem> ancestorList;
                if (!sequenceAncestorMapping.containsKey(ancestor))
                    sequenceAncestorMapping.put(ancestor, new LinkedList<Gem>());

                ancestorList = sequenceAncestorMapping.get(ancestor);
                for (Gem g : gemsToUpdate) {
                    if (!ancestorList.contains(g)) {
                        ancestorList.add(g);
                    }
                }
            }

            gemsToUpdate.clear();
        }

        /*System.out.println("START");
        for (Gem i : lengths.keySet()) System.out.println(i.getLocation().getValue0() + ", " + i.getLocation().getValue1() + " : " + lengths.get(i));
        System.out.println("END");*/

        System.out.println();
        System.out.println();

        /*for (Gem i : lengths.keySet()) {
            if (f.test(lengths.get(i))) { // lengths.get(i) >= 3
                gems.push(i);
            }
        }*/

        // System.out.println("GEMS TO REMOVE");
        // for (Gem i : gems) System.out.println(i.getLocation());
        // System.out.println("END GEMS TO REMOVE");

        /*for (LinkedList<Gem> i : sequenceAncestorMapping.values()) {
            for (Gem j : i) {
                System.out.print(j.getLocation() + ": " + j.getColor() + " | ");
            }
            System.out.println();
        }*/

        return sequenceAncestorMapping.values().stream().filter(i -> f.test(i.size())).collect(Collectors.toCollection(LinkedList::new));
    }

    public LinkedList<LinkedList<Gem>> getSequenceGems(IntPredicate f) {
        // return sequenceGems(reversedPi(DFS().getValue0())).getValue0();
        return sequenceGems(DFS().getValue0(), f);
    }



    /*public HashMap<Gem, Integer> getSequenceLengthGems(){
        return sequenceGems(reversedPi(DFS().getValue0())).getValue1();
    }*/

    // sequenceGems(HashMap<Gem, Gem> pi, f)
    // f -> f >=4, f >= 3, f == 2
}
