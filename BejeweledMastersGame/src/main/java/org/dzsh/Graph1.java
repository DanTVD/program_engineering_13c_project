package org.dzsh;

import org.javatuples.Pair;

import java.util.*;

public class Graph1 {
    private Pair<Integer, Integer> dim;

    private enum Color {
        WHITE, GRAY, BLACK
    }

    public Graph1(Pair<Integer, Integer> dim) {
        this.dim = dim;
    }
    /*private Graph_Node Create_Board_BFS_Graph_Rec(Graph_Node curr, int i, int j) {
        if (i == this.dim.getValue0() && j == this.dim.getValue1())
            return null;

        curr.setRight(Create_Board_BFS_Graph_Rec(curr, i + 1, j));
        curr.setDown(Create_Board_BFS_Graph_Rec(curr, i, j + 1));

        return new Graph_Node(new Graph_Node(Gem.getGem(new Pair<>(i + 1, j))), new Graph_Node(Gem.getGem(new Pair<>(i, j + 1))), Gem.getGem(new Pair<>(i, j)));
    }

    private Graph_Node Create_Board_BFS_Graph_New() {
        return Create_Board_BFS_Graph_Rec(new Graph_Node(new Graph_Node(Gem.getGem(new Pair<>(1, 0))), new Graph_Node(Gem.getGem(new Pair<>(0, 1))), Gem.getGem(new Pair<>(0, 0))), 0, 0);
    }*/

    private Graph_Node Create_Board_BFS_Graph_NewNew(GemTile[][] board) {
        int i, j;
        Graph_Node curr = null, root = null, prevRight = null, prevDown = null, right, down;

        for (i = 0; i < dim.getValue0(); i++) {

            for (j = 0; j < dim.getValue1(); j++) {
                try {
                    right = new Graph_Node(board[i + 1][j].getGem());
                } catch (ArrayIndexOutOfBoundsException e) { right = null; }
                try {
                    down = new Graph_Node(board[i][j + 1].getGem());
                } catch (ArrayIndexOutOfBoundsException e1) { down = null; }

                curr = new Graph_Node(right, down, board[i][j].getGem());
                if (i != 0 && j != 0) prevRight.setRight(curr);

                if (i == 0 & j == 0) root = curr;
                if (j == 0) prevDown = curr;

                prevRight = curr;
            }
            if (i != 0 && j == 0) prevDown.setDown(curr);
        }

        return root;
    }

    private Graph_Node Create_Board_BFS_Graph() {

        Pair<Integer, Integer> ij = new Pair<>(0, 0), ij_right = new Pair<>(1, 0), ij_down = new Pair<>(0, 1);
        int i, j;
        Graph_Node curr, root = null, prev = null;

        for (i = 0; i < this.dim.getValue0(); i++) {
            ij = ij.setAt0(i);
            ij_right = ij_right.setAt0(i + 1);
            for (j = 0; j < this.dim.getValue1(); j++) {
                ij = ij.setAt1(j);
                ij_down = ij_down.setAt1(j + 1);

                curr = new Graph_Node(new Graph_Node(Gem.getGem(ij_right)), new Graph_Node(Gem.getGem(ij_down)), Gem.getGem(ij));
                //System.out.print(curr.getVal().getLocation().getValue0() + ", " + curr.getVal().getLocation().getValue1() + " | ");
                if (prev != null) prev.setDown(curr);

                if (i == 0 & j == 0) root = curr;

                ij = ij.setAt1(j + 1);
                ij_down = ij_down.setAt1(j + 2);
                prev = curr;
            }
            ij = ij.setAt0(i + 1);
            ij_right = ij_right.setAt0(i + 2);
            //System.out.println();
        }

        return root;
    }

    public LinkedList<Gem> Special_BFS(GemTile[][] board) {
        Graph_Node root = Create_Board_BFS_Graph_NewNew(board), curr;

        Stack<Graph_Node> Operations = new Stack<>();

        HashMap<java.awt.Color, LinkedList<Graph_Node>> CombosV = new HashMap<>() {{
            put(java.awt.Color.white, new LinkedList<>());
            put(java.awt.Color.yellow, new LinkedList<>());
            put(java.awt.Color.red, new LinkedList<>());
            put(java.awt.Color.pink, new LinkedList<>());
            put(java.awt.Color.orange, new LinkedList<>());
            put(java.awt.Color.green, new LinkedList<>());
            put(java.awt.Color.blue, new LinkedList<>());
        }},
            CombosH = new HashMap<>() {{
                put(java.awt.Color.white, new LinkedList<>());
                put(java.awt.Color.yellow, new LinkedList<>());
                put(java.awt.Color.red, new LinkedList<>());
                put(java.awt.Color.pink, new LinkedList<>());
                put(java.awt.Color.orange, new LinkedList<>());
                put(java.awt.Color.green, new LinkedList<>());
                put(java.awt.Color.blue, new LinkedList<>());
        }};

        LinkedList<Gem> GemsToRemove = new LinkedList<>(), tempGemsToRemove = new LinkedList<>();

        Map<Gem, Color> Color = new HashMap<>();
        Map<Gem, Integer> Distance = new HashMap<>();
        Map<Gem, Gem> Pi = new HashMap<>();

        curr = root;

        for (Gem gem : Gem.getAllGems()) {
            Color.put(gem, Graph1.Color.WHITE);
            Distance.put(gem, -1);
            Pi.put(gem, null);
        }

        Color.put(curr.getVal(), Graph1.Color.GRAY);
        Distance.put(curr.getVal(), 0);
        Pi.put(curr.getVal(), null);

        Operations.add(curr);
        CombosH.get(curr.getVal().getColor()).push(curr);
        CombosV.get(curr.getVal().getColor()).push(curr);

        while (Operations.size() != 0) {
            curr = Operations.peek();
            System.out.println(curr.getVal().getLocation() + " curr | down " + curr.getDown().getVal().getLocation() + " | right " + curr.getRight().getVal().getLocation());

            if (curr.getDown() != null && Color.get(curr.getDown().getVal()) == Graph1.Color.WHITE) {
                Color.put(curr.getDown().getVal(), Graph1.Color.GRAY);
                Distance.put(curr.getDown().getVal(), 0);
                Pi.put(curr.getDown().getVal(), curr.getVal());

                if (curr.getVal().getColor() == curr.getDown().getVal().getColor()) {
                    CombosV.get(curr.getVal().getColor()).push(curr.getDown());
                } else {
                    System.out.println(curr.getVal().getColor() + " | " + curr.getDown().getVal().getColor());
                    if (CombosV.get(curr.getVal().getColor()).size() >= 3) {
                        for (Graph_Node g : CombosV.get(curr.getVal().getColor())) {
                            GemsToRemove.push(g.getVal());
                            //tempGemsToRemove.sort((o1, o2) -> (o2.getLocation().getValue0() * 10 + o2.getLocation().getValue1()) - (o1.getLocation().getValue0() * 10 + o1.getLocation().getValue1()));
                        }

                        CombosV.get(curr.getVal().getColor()).clear();
                        CombosV.get(curr.getDown().getVal().getColor()).push(curr.getDown());
                    }
                }
                System.out.println(curr.getDown().getVal().getLocation() + " down");
                Operations.add(curr.getDown());

            }

            if (curr.getRight() != null && Color.get(curr.getRight().getVal()) == Graph1.Color.WHITE) {
                Color.put(curr.getRight().getVal(), Graph1.Color.GRAY);
                Distance.put(curr.getRight().getVal(), 0);
                Pi.put(curr.getRight().getVal(), curr.getVal());

                if (curr.getVal().getColor() == curr.getRight().getVal().getColor()) {
                    CombosH.get(curr.getVal().getColor()).push(curr.getRight());
                }
                else {
                    if (CombosH.get(curr.getVal().getColor()).size() >= 3) {
                        for (Graph_Node g : CombosH.get(curr.getVal().getColor()) ) {
                            GemsToRemove.add(g.getVal());
                        }
                    }

                    CombosH.get(curr.getVal().getColor()).clear();
                    CombosH.get(curr.getRight().getVal().getColor()).push(curr.getRight());
                }

                System.out.println(curr.getRight().getVal().getLocation() + " right");
                Operations.add(curr.getRight());
            }

            Operations.pop();
            Color.put(curr.getVal(), Graph1.Color.BLACK);
        }

        System.out.println(Operations.size());

        return GemsToRemove;
    }
}

