package org.dzsh;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import static java.lang.Math.max;

public class Player {
    protected static class CircularIterator<T> implements Iterator<T> {
        private final Collection<T> collection;
        private Iterator<T> iter;
        private T curr = null;

        public CircularIterator(Collection<T> collection) {
            this.collection = collection;
            this.iter = collection.iterator();
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public T next() {
            this.curr = this.iter.next();
            if (!this.iter.hasNext()) this.iter = this.collection.iterator();
            return this.curr;
        }

        public T curr() {
            return this.curr;
        }
    }

    protected Integer score;
    protected int state;
    protected String username;

    protected static int gameRoundsLeft = 3;
    protected static Graph graph;

    protected static LinkedList<Player> players = new LinkedList<>();
    protected static CircularIterator<Player> iterate;

    protected static Board board;
    /*
    public final static Iterator<Player> iterate = new Iterator<Player>() {
        private Iterator<Player> iter = Player.players.iterator();
        private Player curr = null;

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public Player next() {
            if (!this.iter.hasNext()) Player.players.iterator();
            this.curr = this.iter.next();
            return this.curr;
        }

        public Player curr() {
            return this.curr;
        }
    };


     */
    /*
       public final static PrivilegedAction<Player> iterate = (new PrivilegedAction<PrivilegedAction<Player>>() {
        @Override
        public PrivilegedAction<Player> run() {
            AtomicReference<Iterator<Player>> iterator = new AtomicReference<>(Player.players.iterator());

            PrivilegedAction<Player> iter = new PrivilegedAction<Player>() {
                @Override
                public Player run() {
                    if (!iterator.get().hasNext()) iterator.set(Player.players.iterator());
                    return iterator.get().next();
                }
            };

            return iter;
        }
    }).run();
     */

    public Player() {
        this.score = 0;
        this.username = "";
        this.state = 2;

        Player.players.add(this);
    }

    public Player(String username) {
        this();
        this.username = username;
    }

    public boolean play() {
        //HashMap<Player, Integer> state = new HashMap<>();
        //System.out.println("State Creation");
        /*for (Player i : Player.values()) {
            state.put(i, 2);
        }*/
        //System.out.println("State Init");

        //Board that = this;

        //return entry -> {

        System.out.println("Round " + (this.state % 2 + 1));

        if (Player.graph.getSequenceGems(Graph.findBonus).size() != 0) {
            System.out.println("IF2");
            this.state += 1;
        }

        boolean ret = this.state != 0;

        System.out.println("State Before: " + this.state);
        this.state--;
        System.out.println("State After: " + this.state);

        if (this.state == 0) {
            System.out.println("IF1");
            // this.state = 2;
            Player.iterate.next();

            Player.iterate.curr().onPlayerShift();

            if (Player.isGameRoundOver()) {

                for (Player i : Player.players)
                    i.state = 2;

                Player.gameRoundsLeft--;
            }

            return false;
        }

        // System.out.println("Round " + state.get(entry));

        return ret;
        //};
    }

    protected void onPlayerShift() {
        Player.board.setDisabled(true);

        Player.board.setDisabled(false);
    }

    public static boolean isGameRoundOver() {
        for (Player player : Player.players) {
            if (player.state != 0) return false;
        }
        return true;
    }

    public static int getGameRoundsLeft() {
        return gameRoundsLeft;
    }

    public Player whoPlaysNext() {
        return Player.iterate.curr();
    }

    public static void setGraph(Graph graph) {
        Player.graph = graph;
    }

    public Integer getPlayerScore() {
        return score;
    }

    public void addPlayerScore(Integer score) {
        this.score += score;
    }

    public int getState() {
        return state;
    }

    /*public void setState(int state) {
        this.state = state;
    }*/

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static void ready() {
        Player.iterate = new CircularIterator<>(Player.players);
    }

    public static void setBoard(Board board) {
        Player.board = board;
    }

    public static Player getCurrPlayer() {
        return Player.iterate.curr();
    }

    /*public static void setGameRoundsLeft(int gameRoundsLeft) {
        Player.gameRoundsLeft = gameRoundsLeft;
    }*/
}
