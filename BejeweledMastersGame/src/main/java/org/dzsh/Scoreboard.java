package org.dzsh;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;

public class Scoreboard {

    private TableView<UserScore> scoreTable;
    private ObservableList<UserScore> userScores;

    private SQLite sqLite;

    private static final String createTable = "CREATE TABLE IF NOT EXISTS Scores (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "username TEXT NOT NULL," +
            "score INTEGER NOT NULL" +
            ")";
    private static final String selectTable = "SELECT * FROM Scores";
    //private static Scoreboard instance;

    /*private*/ public Scoreboard(TableView<UserScore> scoreTable) {
        this.scoreTable = scoreTable;

        this.sqLite = new SQLite();
        this.sqLite.query(createTable);

        this.userScores = FXCollections.observableArrayList();
        this.setScoresAtTable();
    }

    /*public static Scoreboard getInstance() {
        if (Scoreboard.instance == null)
            Scoreboard.instance = new Scoreboard();
        return Scoreboard.instance;
    }*/

    public void setScoreTable(TableView<UserScore> scoreTable) {
        this.scoreTable = scoreTable;
    }

    public void setScoresAtTable() {

        ResultSet rs = this.sqLite.queryWithResult(selectTable);
        try {
            while (rs.next()) {
                this.userScores.add(new UserScore(rs.getInt("id"), rs.getString("username"), rs.getInt("score")));
            }
        } catch (Exception e) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }

        this.sqLite.closeQueryWithResult();

        if (scoreTable != null) this.scoreTable.setItems(this.userScores);
    }

    public void setNewScore(String username, int score) {
        String insertTable = MessageFormat.format("INSERT INTO Scores (username, score)" +
                "VALUES ({0}, {1})", username, score);

        this.sqLite.query(insertTable);
    }

    public void openDB() {
        this.sqLite.openDB();
    }
    public void closeDB() {
        this.sqLite.closeDB();
    }
}
