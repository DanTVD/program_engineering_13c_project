package org.dzsh;

import java.sql.*;

public class SQLite {
    private Connection c;
    private Statement stmt;
    private ResultSet rs;

    public SQLite() {
        this.c = null;
        this.stmt = null;
        this.rs = null;

        openDB();
    }

    public void openDB() {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:./scores.db");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    public void closeDB() {
        try {
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        System.out.println("Closed database successfully");
    }


    public void query(String sql) {
        stmt = null;

        try {
            //c.setAutoCommit(false);

            stmt = c.createStatement();
            stmt.executeUpdate(sql);

            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // System.exit(0);
        }
    }

    public ResultSet queryWithResult(String sql) {
        stmt = null;

        try {

            stmt = c.createStatement();
            rs = stmt.executeQuery(sql);

            /*while ( rs.next() ) {
                //int id = rs.getInt("id");
                //String  name = rs.getString("name");
                //int age  = rs.getInt("age");
                //String  address = rs.getString("address");
                //float salary = rs.getFloat("salary");
            }*/


        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // System.exit(0);
        }
        System.out.println("Operation done successfully");
        return rs;
    }

    public void closeQueryWithResult() {
        try {
            rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            // System.exit(0);
        }
    }


}


