package org.dzsh;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
// import javafx.util.Pair;
import org.javatuples.Pair;

import java.awt.*;
import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;

public class Board extends Logic_Board implements EventHandler<ActionEvent> {
    private GemTile[][] visualBoard;
    private GemTile chosenTile;

    //private Callable<Color> randomColor;

    private Label roundCounter;
    private Label[] playersData;

    private GridPane boardLayout;
    private HBox scoreLayout;
    private HBox roundLayout;
    private BorderPane layout;

    private Stage window;
    private Scene scene;

    private String customUsername;

    private Scoreboard scoreboard;

    private Thread thread;
    private Object lock;

    // private Pair<Integer, Integer> dim;
    private Graph graph;

    //private Logic_Board logic_board;

    public Board(HBox roundLayout, HBox scoreLayout, GridPane boardLayout, BorderPane layout, Scoreboard scoreboard, String customUsername, Stage window, Scene scene, Thread thread, Object lock, Pair<Integer, Integer> dim) {

        super(dim, new Graph());

        //this.randomColor = getRandom(GemTile.getColorImageMap());

        // this.dim = Gem.dim = dim;
        this.visualBoard = new GemTile[dim.getValue0()][dim.getValue1()];

        this.boardLayout = boardLayout;
        this.scoreLayout = scoreLayout;
        this.roundLayout = roundLayout;
        this.layout = layout;
        this.window = window;
        this.scene = scene;

        this.thread = thread;
        this.lock = lock;

        this.customUsername = customUsername;

        if (this.customUsername.length() != 0)
            this.players[Player.Human.ordinal()].setUsername(this.customUsername);

        this.scoreboard = scoreboard;

        //this.graph = new Graph(); //this.dim

        // this.logic_board = new Logic_Board(GetBoardCopy(), dim, graph);

        this.roundCounter = new Label(MessageFormat.format("{0} Rounds Left", org.dzsh.Player.getGameRoundsLeft()));
        this.roundLayout.getChildren().addAll(roundCounter);

        this.playersData = new javafx.scene.control.Label[Player.values().length];

        for (int i = 0; i < this.playersData.length; i++) {
            playersData[i] = new Label(MessageFormat.format("{0}\nScore: 0\nTurn: 0/2", this.customUsername.length() == 0 || i != 1? Player.values()[i].name() : this.customUsername));
        }

        this.scoreLayout.getChildren().addAll(playersData);

        //playersData[0] = new javafx.scene.control.Label("COM\nScore: 0\nTurn: 0/2");
        //playersData[1] = new Label("USR\nScore: 0\nTurn: 0/2");

        for (int i = 0; i < visualBoard.length; i++) {
            for (int j = 0; j < visualBoard[i].length; j++) {
                try {
                    visualBoard[i][j] = new GemTile(super.board[i][j].getColor(), i, j, "");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                visualBoard[i][j].setOnAction(this);
                GridPane.setConstraints(visualBoard[i][j], j, i);
            }
            boardLayout.getChildren().addAll(visualBoard[i]);
        }

        org.dzsh.Player.setBoard(this);

    }

    public Board(Board other) {
        super(other);
        this.dim = other.dim;
        this.window = other.window;

        this.boardLayout = new GridPane();
        boardLayout.setPadding(new Insets(10, 10, 10, 10));
        boardLayout.setVgap(8);
        boardLayout.setHgap(10);

        this.scoreLayout = new HBox();
        scoreLayout.setPadding(new Insets(10, 10, 10, 10));
        scoreLayout.setSpacing(220);

        roundLayout = new HBox();
        roundLayout.setPadding(new Insets(10, 10, 10, 10));
        roundLayout.setAlignment(Pos.CENTER);

        this.layout = new BorderPane();
        layout.setTop(roundLayout);
        layout.setCenter(scoreLayout);
        layout.setBottom(boardLayout);

        this.scene = new Scene(layout, 350, 370);

        this.visualBoard = new GemTile[other.board.length][other.board[0].length];

        this.customUsername = other.customUsername;

        this.scoreboard = other.scoreboard;

        this.roundCounter = new Label(MessageFormat.format("{0} Rounds Left", org.dzsh.Player.getGameRoundsLeft()));
        this.roundLayout.getChildren().addAll(roundCounter);

        this.playersData = new javafx.scene.control.Label[Player.values().length];
        //int[] scores = new int[Player.values().length];
        //scores[Player.Computer.ordinal()] = other.getComputerScore();
        //scores[Player.Human.ordinal()] = other.getPlayerScore();

        for (int i = 0; i < this.playersData.length; i++) {
            this.playersData[i] = new Label(MessageFormat.format("{0}\nScore: {1}\nTurn: {2}/2", this.players[i].getUsername(), this.players[i].getPlayerScore(), other.players[i].getState()));
        }

        scoreLayout.getChildren().addAll(playersData);

        for (int i = 0; i < visualBoard.length; i++) {
            for (int j = 0; j < visualBoard[i].length; j++) {
                try {
                    visualBoard[i][j] = new GemTile(super.board[i][j].getColor(), i, j, "");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                visualBoard[i][j].setOnAction(this);
                GridPane.setConstraints(visualBoard[i][j], j, i);
            }
            boardLayout.getChildren().addAll(visualBoard[i]);
        }

        //this.randomColor = other.randomColor;

        //this.logic_board = other.logic_board;

        this.thread = other.thread;

        this.lock = other.lock;

        this.graph = other.graph;

        window.setScene(scene);

        org.dzsh.Player.setBoard(this);
    }

    public void setDisabled(boolean flag) {
        //this.boardLayout.setDisable(flag);
        this.boardLayout.setMouseTransparent(flag);
        this.boardLayout.setFocusTraversable(!flag);
    }


/*
    public GemTile[][] GetBoardCopy() {
        GemTile[][] newBoard = new GemTile[this.board.length][this.board[0].length];

        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[i].length; j++) {
                newBoard[i][j] = this.board[i][j];
            }
        }

        return newBoard;
    }

 */

    protected void swap(Pair<Integer, Integer> src, Pair<Integer, Integer> dst) {
        GemTile tmp = this.visualBoard[dst.getValue0()][dst.getValue1()]; // tmp = dst

        this.visualBoard[dst.getValue0()][dst.getValue1()].setGraphic(new ImageView(this.visualBoard[src.getValue0()][src.getValue1()].getTileImage()));
        this.visualBoard[dst.getValue0()][dst.getValue1()] = this.visualBoard[src.getValue0()][src.getValue1()]; // dst = src

        this.visualBoard[src.getValue0()][src.getValue1()].setGraphic(new ImageView(tmp.getTileImage()));
        this.visualBoard[src.getValue0()][src.getValue1()] = tmp; // src = tmp

        // this.board[dst.getKey()][dst.getValue()].setLocation(src);
        // this.board[src.getKey()][src.getValue()].setLocation(dst);
        super.swap(src, dst);
    }

    @Override
    protected void shuffle() {
        synchronized (this.lock) {
            lock.notify();
        }

        super.shuffle();
    }

    /*public LinkedList<HashMap<Gem, Integer>> getCombinationsLength(){
        LinkedList<HashMap<Gem, Integer>> gemsLength = new LinkedList<>();

        Gem.dir = false;
        gemsLength.push(this.graph.getSequenceLengthGems());

        Gem.dir = true;
        gemsLength.push(this.graph.getSequenceLengthGems());

        return gemsLength;
    }*/

    @Override
    public int turn(Gem a, Gem b, BiConsumer<Gem.Color, Pair<Integer, Integer>> sideEffect) {
        synchronized (this.lock) {
            lock.notify();
        }

        Board that = this;
        return super.turn(a, b, new BiConsumer<Gem.Color, Pair<Integer, Integer>>() {
            @Override
            public void accept(Gem.Color color, Pair<Integer, Integer> objects) {
                that.visualBoard[objects.getValue0()][objects.getValue1()].updateColor(color);
            }
        });
    }

    private void insertUserScore() {
        String username = "";

        username = this.customUsername.length() != 0 ? this.customUsername : "User";

        this.scoreboard.setNewScore(username, this.players[Player.Human.ordinal()].getPlayerScore());
    }

    public boolean gameFinish() {
        if (this.isGameOver()) {
            this.scoreboard.setNewScore(this.players[Player.Human.ordinal()].getUsername(), this.players[Player.Human.ordinal()].getPlayerScore());

            Alert gameOverMsg = new Alert(Alert.AlertType.INFORMATION);
            gameOverMsg.setHeaderText(MessageFormat.format("Game is Over!\n{0} Scored {1} Points\n{2} Won!\n(Will Return Back to Main Menu)", this.customUsername.length() == 0 ? Player.Human.name() : this.customUsername,
                    this.players[Player.Human.ordinal()].getPlayerScore(), this.players[Player.Human.ordinal()].getPlayerScore() > this.players[Player.Computer.ordinal()].getPlayerScore() ? this.players[Player.Human.ordinal()].getUsername() : this.players[Player.Computer.ordinal()].getUsername()));

            gameOverMsg.showAndWait().ifPresent(type -> {});
            this.scoreboard.closeDB();
            return true;
        } else {
            Alert isExitOngoingGame = new Alert(Alert.AlertType.CONFIRMATION);
            isExitOngoingGame.setHeaderText("There is a Game in Progress\nAre you sure You Want to Exit The Game Board?\nAll Progress will be lost\n(Will Return Back to Main Menu)");

            ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);

            isExitOngoingGame.getButtonTypes().setAll(yesButton, noButton);

            AtomicBoolean isExitOngoingGameFlag = new AtomicBoolean(false);

            isExitOngoingGame.showAndWait().ifPresent(type -> {
                if (type.getButtonData() == ButtonBar.ButtonData.YES) {
                    isExitOngoingGameFlag.set(true);
                }
            });

            this.scoreboard.closeDB();
            return isExitOngoingGameFlag.get();
        }
    }

    @Override
    public void handle(ActionEvent event) {
        if (chosenTile == null) {
            chosenTile = (GemTile) event.getSource();
        } else {
            //try {
                int addedScore = turn(chosenTile.getGem(), ((GemTile) event.getSource()).getGem(), null);
                if (addedScore > 0 && this.players[Player.Human.ordinal()].play()) {
                    this.players[Player.Human.ordinal()].addPlayerScore(addedScore);

                    // int addedScore = turn(chosenTile.getGem(), ((GemTile) event.getSource()).getGem());
                    // System.out.println("You earned " + addedScore + " points!");
                    // this.playerScore += addedScore;

                    System.out.println("SCORE: " + this.players[Player.Human.ordinal()].getPlayerScore());
                } else if (addedScore == 0) {
                    System.out.println("Not a Match");
                } else {
                    System.out.println("ROUND OVER");

                    this.boardLayout.setDisable(true);



                    this.boardLayout.setDisable(false);

                    if (this.isGameOver())
                        gameFinish();
                }


            /*} catch (IOException e) {
                e.printStackTrace();
            }*/

            /*LinkedList<HashMap<Gem, Integer>> gemLength = this.getCombinationsLength();

            for (int i = 0; i < this.board.length; i++)
            {
                for (int j = 0; j < this.board[i].length; j++){
                    board[i][j].setText(gemLength.get(1).get(board[i][j].getGem()).toString() + " | " + gemLength.get(0).get(board[i][j].getGem()).toString());

                    if (Color.white.equals(board[i][j].getGem().getColor())) {
                        System.out.print("W ");
                    } else if (Color.yellow.equals(board[i][j].getGem().getColor())) {
                        System.out.print("Y ");
                    } else if (Color.red.equals(board[i][j].getGem().getColor())) {
                        System.out.print("R ");
                    } else if (Color.pink.equals(board[i][j].getGem().getColor())) {
                        System.out.print("P ");
                    } else if (Color.orange.equals(board[i][j].getGem().getColor())) {
                        System.out.print("O ");
                    } else if (Color.green.equals(board[i][j].getGem().getColor())) {
                        System.out.print("G ");
                    }
                }
                System.out.println();
            }
            System.out.println();*/

            ((GemTile) event.getSource()).setSelected(false);
            chosenTile.setSelected(false);
            chosenTile = null;
        }
    }
}
