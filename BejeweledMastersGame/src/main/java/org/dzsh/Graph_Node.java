package org.dzsh;

public class Graph_Node {
    private Graph_Node Right;
    private Graph_Node Down;
    private Gem Val;


    public Graph_Node() {
        this.Right = null;
        this.Down = null;
        this.Val = null;
    }

    public Graph_Node(Graph_Node right, Graph_Node down, Gem val) {
        this.Right = right;
        this.Down = down;
        this.Val = val;
    }

    public Graph_Node(Gem val) {
        this.Right = null;
        this.Down = null;
        this.Val = val;
    }

    public Graph_Node getRight() {
        return Right;
    }

    public void setRight(Graph_Node right) {
        Right = right;
    }

    public Graph_Node getDown() {
        return Down;
    }

    public void setDown(Graph_Node down) {
        Down = down;
    }

    public Gem getVal() {
        return Val;
    }

    public void setVal(Gem val) {
        Val = val;
    }
}
