package org.dzsh;

// import javafx.util.Pair;
import org.javatuples.Pair;

//import java.awt.*;
//import java.awt.List;
import java.util.*;

public class Gem {
    enum Color {
        WHITE, YELLOW, RED, PURPLE, ORANGE, GREEN, BLUE;

        private static final Color[] VALUES = values();
        private static final int SIZE = VALUES.length;
        private static final Random RANDOM = new Random();

        public static Color random()  {
            return (Color) VALUES[RANDOM.nextInt(SIZE)];
        }

        public static Color[] getColors() {

            return VALUES;
        }
    }

    private Color color;
    private Pair<Integer, Integer> location;
    private static HashMap<Pair<Integer, Integer>, Gem> map = new HashMap<>();
    public static Pair<Integer, Integer> dim;
    public static boolean dir = false; // true = up / down || false = left / right

    public Gem(Color color, int row, int col) {
        this.color = color;
        this.location = new Pair<>(row, col);

        Gem.map.put(this.location, this);
    }

    public Pair<Integer, Integer> getLocation() {
        return location;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public static Gem getGem(Pair<Integer, Integer> loc) {
        return map.get(loc);
    }

    public static void swap(Pair<Integer, Integer> loc1, Pair<Integer, Integer> loc2) {
        Gem tmp;

        tmp = Gem.map.get(loc1);
        Gem.map.put(loc1, Gem.map.get(loc2));
        Gem.map.put(loc2, tmp);

        Gem.map.get(loc1).location = loc1;
        Gem.map.get(loc2).location = loc2;
    }

    public ArrayList<Gem> adj() {
        ArrayList<Gem> arr = new ArrayList<>();
        Pair<Integer, Integer> loc = this.getLocation();

        if (Gem.dir) {
            if (loc.getValue0() - 1 >= 0) {
                if (Gem.getGem(new Pair<>(loc.getValue0() - 1, loc.getValue1())).getColor() == this.getColor()) {
                    if (Gem.getGem(new Pair<>(loc.getValue0() - 1, loc.getValue1())).getColor() != null)
                        arr.add(Gem.getGem(new Pair<>(loc.getValue0() - 1, loc.getValue1())));
                }
            }

            if (loc.getValue0() + 1 < Gem.dim.getValue1()) {
                if (Gem.getGem(new Pair<>(loc.getValue0() + 1, loc.getValue1())).getColor() == this.getColor()) {
                    if (Gem.getGem(new Pair<>(loc.getValue0() + 1, loc.getValue1())).getColor() != null)
                        arr.add(Gem.getGem(new Pair<>(loc.getValue0() + 1, loc.getValue1())));
                }
            }
        } else {
            if (loc.getValue1() - 1 >= 0) {
                if (Gem.getGem(new Pair<>(loc.getValue0(), loc.getValue1() - 1)).getColor() == this.getColor()) {
                    if (Gem.getGem(new Pair<>(loc.getValue0(), loc.getValue1() - 1)).getColor() != null)
                        arr.add(Gem.getGem(new Pair<>(loc.getValue0(), loc.getValue1() - 1)));
                }
            }

            if (loc.getValue1() + 1 < Gem.dim.getValue0()) {
                if (Gem.getGem(new Pair<>(loc.getValue0(), loc.getValue1() + 1)).getColor() == this.getColor()) {
                    if (Gem.getGem(new Pair<>(loc.getValue0(), loc.getValue1() + 1)).getColor() != null)
                        arr.add(Gem.getGem(new Pair<>(loc.getValue0(), loc.getValue1() + 1)));
                }
            }
        }
        return arr;
    }

    public static Collection<Gem> getAllGems() {
        return Gem.map.values();
    }
}
