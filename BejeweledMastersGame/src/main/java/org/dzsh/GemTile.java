package org.dzsh;

import javafx.collections.ObservableArray;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Pair;

//import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class GemTile extends ToggleButton {
    private Gem gem;
    private javafx.scene.image.Image tileImage;
    private static final Map<Gem.Color, String> COLOR_IMAGE_MAP = new HashMap<>() {{
        put(Gem.Color.WHITE, "file:./Assets/Gems/gem_white.png");
        put(Gem.Color.YELLOW, "file:./Assets/Gems/gem_yellow.png");
        put(Gem.Color.RED, "file:./Assets/Gems/gem_red.png");
        put(Gem.Color.PURPLE, "file:./Assets/Gems/gem_purple.png");
        put(Gem.Color.ORANGE, "file:./Assets/Gems/gem_orange.png");
        put(Gem.Color.GREEN, "file:./Assets/Gems/gem_green.png");
        put(Gem.Color.BLUE, "file:./Assets/Gems/gem_blue.png");
    }};


    public GemTile(Gem.Color color, int row, int col, String text) throws FileNotFoundException {
        super();
        this.gem = new Gem(color, row, col);
        this.setContentDisplay(ContentDisplay.LEFT);
        this.setText(text);
        updateColor(color);
    }

    public static Map<Gem.Color, String> getColorImageMap() {
        return COLOR_IMAGE_MAP;
    }

    public Image getTileImage() {
        return tileImage;
    }

    public void setTileImage(Image tileImage) {
        this.tileImage = tileImage;
    }

    public void updateColor(Gem.Color color) {
        this.tileImage = new javafx.scene.image.Image(COLOR_IMAGE_MAP.get(color), 20.0, 20.0, true, true, false);
        this.setGraphic(new ImageView(this.tileImage));
        this.gem.setColor(color);
    }

    public Gem getGem() {
        return gem;
    }
}

