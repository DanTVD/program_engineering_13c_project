package org.dzsh;

import org.javatuples.Pair;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Set;

public class Bot extends Player {

    public Bot() {
        super();
    }

    public Bot (String username) {
        super(username);
    }

    public Pair<Integer, Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> minmax(Logic_Board board, Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> move, int depth) {
        System.out.println("Entr Recur | " + depth);
        if (Player.isGameRoundOver()) { // depth == -1
            // return static evaluation of board
            System.out.println("IF1");
            return new Pair<>(0, move);
        } else {
            System.out.println("ELSE");
            int maxEval = Integer.MIN_VALUE, score;
            Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> maxMove = null;
            Pair<Integer, Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> eval = null;

            for (Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> child : board.potentialSequences()) {
                Logic_Board tmp = new Logic_Board(board);
                tmp.setGemsFall(false);

                if (depth == 0) { // leaf
                    move = child;
                } else {
                    eval = minmax(tmp, child, depth - 1);
                }

                // int score = ( move == null ? 0 : tmp.turn(Gem.getGem(move.getValue0()), Gem.getGem(move.getValue1())) );
                if (move == null) { // initial call
                    score = 0;
                } else {
                    score = tmp.turn(Gem.getGem(move.getValue0()), Gem.getGem(move.getValue1()));
                }

                // eval = depth == 0 ? new Pair<>(0, null) : minmax(tmp, child, depth - 1);
                // maxEval = Math.max(maxEval, eval.getValue0() + score);

                if (depth == 0) eval = new Pair<>(score, child);

                if (maxEval <= eval.getValue0() + score) {
                    System.out.println("ELSE IF2");
                    maxEval = eval.getValue0() + score;
                    maxMove = move;
                }
                
            }

            System.out.println(new Pair<>(maxEval, maxMove));
            return new Pair<>(maxEval, maxMove);
        }
    }

    protected void onPlayerShift() {
        Player.board.setDisabled(true);
        Pair<Integer, Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> gemsToSwitch = minmax(Player.board, null, 2);
        System.out.println(gemsToSwitch.getValue1());
        this.addPlayerScore(Player.board.turn(Gem.getGem(gemsToSwitch.getValue1().getValue0()), Gem.getGem(gemsToSwitch.getValue1().getValue1())));
        Player.board.setDisabled(false);
    }
}
