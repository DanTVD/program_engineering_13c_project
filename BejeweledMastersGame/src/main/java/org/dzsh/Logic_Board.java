package org.dzsh;

import org.javatuples.Pair;

import java.util.*;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class Logic_Board {
    protected Gem[][] board;
    protected Pair<Integer, Integer> dim;

    private Graph graph;

    //private Integer playerScore;
    //private Integer computerScore;

    public enum Player {Computer, Human};
    //protected HashMap<Logic_Board.Player, Integer> state;

    //protected int gameRoundsLeft;

    protected org.dzsh.Player[] players = new org.dzsh.Player[Player.values().length];

    private boolean isGemsFall = true;

    public Logic_Board(Pair<Integer, Integer> dim, Graph graph) {

        this.dim = Gem.dim = dim;
        this.board = new Gem[dim.getValue0()][dim.getValue1()];

        /*for (int i = 0; i < dim.getValue0(); i++) {
            for (int j = 0; j < dim.getValue1(); j++) {
                this.board[i][j] = board[i][j].getGem();
            }
        }

         */

        this.graph = graph;

        //this.playerScore = 0;
        //this.computerScore = 0;

        //this.gameRoundsLeft = 3;

        //this.state = new HashMap<>();

        /*for (Player i : Player.values()) {
            state.put(i, 2);
        }*/

        for (int i = 0; i < Player.values().length; i++) {
            if (Player.values()[i].name().equals(Player.Computer.name()))
                this.players[i] = new org.dzsh.Bot(Player.values()[i].name());
            else {
                this.players[i] = new org.dzsh.Player(Player.values()[i].name());
            }


        }

        org.dzsh.Player.setGraph(graph);

        org.dzsh.Player.ready();

        Random random = new Random();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                try {
                    // board[i][j] = new GemTile(this.randomColor.call(), i, j);
                    // Map m = new HashMap(GemTile.getColorImageMap());

                    LinkedList<Gem.Color> m = new LinkedList<>(Arrays.asList(Gem.Color.getColors())); //Arrays.asList(Gem.Color.getColors());

                    if ((i >= 2) && (board[i - 1][j].getColor() == board[i - 2][j].getColor())) {
                        m.remove(board[i - 1][j].getColor());
                    }
                    if ((j >= 2) && (board[i][j - 1].getColor() == board[i][j - 2].getColor())) {
                        m.remove(board[i][j - 1].getColor());
                    }

                    /*System.out.println( i + " ," + j + ": " + m.values());
                    System.out.println();*/

                    this.board[i][j] = new Gem(m.get(random.nextInt(m.size())), i, j); // Gem.Color.random()

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
        //this.printBoard();

    }

    public Logic_Board(Pair<Integer, Integer> dim, Graph graph, boolean isGemsFall) {
        this(dim, graph);
        this.isGemsFall = isGemsFall;
    }

    /*
    private static <K, V> Callable<K> getRandom(Map<K, V> m) {
        java.util.List<K> keysAsArray = new ArrayList<K>(m.keySet());
        Random r = new Random();

        Callable<K> random = () -> {
            return keysAsArray.get(r.nextInt(keysAsArray.size()));
        };

        return random;
    }
    */

    public Logic_Board(Logic_Board other) {
        this.dim = other.dim;
        this.board = new Gem[other.board.length][other.board[0].length];

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                this.board[i][j] = new Gem(other.board[i][j].getColor(), i, j);
            }
        }

        this.graph = other.graph;

        this.isGemsFall = other.isGemsFall;

        //this.playerScore = other.playerScore;
        //this.computerScore = other.computerScore;

        //this.state = other.state;

        //this.gameRoundsLeft = other.gameRoundsLeft;
        this.players = other.players;
    }

    public Set<Gem> getCombinations() {
        Set<Gem> gemsToRemove = new LinkedHashSet<Gem>();

        //gemsToRemove.push(this.graph.Special_BFS(this.GetBoardCopy()));

        Gem.dir = false;
        gemsToRemove.addAll(this.graph.getSequenceGems(Graph.findMatch).stream().flatMap(List::stream).collect(Collectors.toList()));

        Gem.dir = true;
        gemsToRemove.addAll(this.graph.getSequenceGems(Graph.findMatch).stream().flatMap(List::stream).collect(Collectors.toList()));

        return gemsToRemove;
    }

    public boolean isSequence() {
        return potentialSequences().size() != 0;
    }

    public LinkedList<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> potentialSequences() {
        Gem min, max;
        LinkedList<LinkedList<Gem>> lists;
        LinkedList<Pair<Pair<Integer, Integer>, Pair<Integer, Integer>>> swapGems = new LinkedList<>();

        Comparator<Gem> comparatorHor = new Comparator<Gem>() {
            @Override
            public int compare(Gem o1, Gem o2) {
                return o1.getLocation().getValue0() - o2.getLocation().getValue0();
            }
        };

        Comparator<Gem> comparatorVer = new Comparator<Gem>() {
            @Override
            public int compare(Gem o1, Gem o2) {
                return o1.getLocation().getValue1() - o2.getLocation().getValue1();
            }
        };

        Gem.dir = false;
        lists = this.graph.getSequenceGems(Graph.findPotential);

        for (LinkedList<Gem> l : lists) {
            // l.sort(comparatorHor);
            min = l.stream().min(comparatorHor).orElseThrow(NoSuchElementException::new);
            max = l.stream().max(comparatorHor).orElseThrow(NoSuchElementException::new);

            Pair<Integer, Integer> rightFromMax = new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1());

            if ((max.getLocation().getValue0() + 2 < this.dim.getValue1()) && (Gem.getGem(new Pair<>(max.getLocation().getValue0() + 2, max.getLocation().getValue1())).getColor() == max.getColor()))
                swapGems.add(new Pair<>(new Pair<>(max.getLocation().getValue0() + 2, max.getLocation().getValue1()), rightFromMax));
            if ((max.getLocation().getValue0() + 1 < this.dim.getValue1()) && (max.getLocation().getValue1() + 1 < this.dim.getValue0()) && (Gem.getGem(new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1() + 1)).getColor() == max.getColor()))
                swapGems.add(new Pair<>(new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1() + 1), rightFromMax));
            if ((max.getLocation().getValue0() + 1 < this.dim.getValue1()) && (max.getLocation().getValue1() - 1 >= 0) && (Gem.getGem(new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1() - 1)).getColor() == max.getColor()))
                swapGems.add(new Pair<>(new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1() - 1), rightFromMax));

            Pair<Integer, Integer> leftFromMin = new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1());

            if ((min.getLocation().getValue0() - 2 >= 0) && (Gem.getGem(new Pair<>(min.getLocation().getValue0() - 2, min.getLocation().getValue1())).getColor() == min.getColor()))
                swapGems.add(new Pair<>(new Pair<>(min.getLocation().getValue0() - 2, min.getLocation().getValue1()), leftFromMin));
            if ((min.getLocation().getValue0() - 1 >= 0) && (min.getLocation().getValue1() + 1 < this.dim.getValue0()) && (Gem.getGem(new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1() + 1)).getColor() == min.getColor()))
                swapGems.add(new Pair<>(new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1() + 1), leftFromMin));
            if ((min.getLocation().getValue0() - 1 >= 0) && (min.getLocation().getValue1() - 1 >= 0) && (Gem.getGem(new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1() - 1)).getColor() == min.getColor()))
                swapGems.add(new Pair<>(new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1() - 1), leftFromMin));
        }

        Gem.dir = true;
        lists = this.graph.getSequenceGems(Graph.findPotential);

        for (LinkedList<Gem> l : lists) {
            min = l.stream().min(comparatorVer).orElseThrow(NoSuchElementException::new);
            max = l.stream().max(comparatorVer).orElseThrow(NoSuchElementException::new);

            Pair<Integer, Integer> upFromMin = new Pair<>(min.getLocation().getValue0(), min.getLocation().getValue1() - 1);

            if ((min.getLocation().getValue1() - 2 >= 0) && Gem.getGem(new Pair<>(min.getLocation().getValue0(), min.getLocation().getValue1() - 2)).getColor() == min.getColor())
                swapGems.add(new Pair<>(new Pair<>(min.getLocation().getValue0(), min.getLocation().getValue1() - 2), upFromMin));
            if ((min.getLocation().getValue0() + 1 < this.dim.getValue1()) && (min.getLocation().getValue1() - 1 >= 0) && Gem.getGem(new Pair<>(min.getLocation().getValue0() + 1, min.getLocation().getValue1() - 1)).getColor() == min.getColor())
                swapGems.add(new Pair<>(new Pair<>(min.getLocation().getValue0() + 1, min.getLocation().getValue1() - 1), upFromMin));
            if ((min.getLocation().getValue0() - 1 >= 0) && (min.getLocation().getValue1() - 1 >= 0) && Gem.getGem(new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1() - 1)).getColor() == min.getColor())
                swapGems.add(new Pair<>(new Pair<>(min.getLocation().getValue0() - 1, min.getLocation().getValue1() - 1), upFromMin));

            Pair<Integer, Integer> downFromMax = new Pair<>(max.getLocation().getValue0(), max.getLocation().getValue1() + 1);

            if ((max.getLocation().getValue1() + 2 < this.dim.getValue0()) && Gem.getGem(new Pair<>(max.getLocation().getValue0(), max.getLocation().getValue1() + 2)).getColor() == max.getColor())
                swapGems.add(new Pair<>(new Pair<>(max.getLocation().getValue0(), max.getLocation().getValue1() + 2), downFromMax));
            if ((max.getLocation().getValue0() - 1 >= 0) && (max.getLocation().getValue1() + 1 < this.dim.getValue0()) && Gem.getGem(new Pair<>(max.getLocation().getValue0() - 1, max.getLocation().getValue1() + 1)).getColor() == max.getColor())
                swapGems.add(new Pair<>(new Pair<>(max.getLocation().getValue0() - 1, max.getLocation().getValue1() + 1), downFromMax));
            if ((max.getLocation().getValue0() + 1 < this.dim.getValue1()) && (max.getLocation().getValue1() + 1 < this.dim.getValue0()) && Gem.getGem(new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1() + 1)).getColor() == max.getColor())
                swapGems.add(new Pair<>(new Pair<>(max.getLocation().getValue0() + 1, max.getLocation().getValue1() + 1), downFromMax));
        }
        return swapGems;
    }

    protected void swap(Pair<Integer, Integer> src, Pair<Integer, Integer> dst) {
        Gem a = this.board[src.getValue0()][src.getValue1()];
        Gem b = this.board[dst.getValue0()][dst.getValue1()];

        this.board[src.getValue0()][src.getValue1()] = b;
        this.board[dst.getValue0()][dst.getValue1()] = a;

        Gem.swap(src, dst);
    }

    public int turn(Gem a, Gem b, BiConsumer<Gem.Color, Pair<Integer, Integer>> sideEffect) {
        int score = 0;

        boolean rowDiff = Math.abs(a.getLocation().getValue0() - b.getLocation().getValue0()) == 1,
                colDiff = Math.abs(a.getLocation().getValue1() - b.getLocation().getValue1()) == 1;
        if ((rowDiff || colDiff) && !(rowDiff && colDiff)) {
            Pair<Integer, Integer> src = a.getLocation(), dst = b.getLocation();

            this.swap(src, dst);

            Set<Gem> gemsToRemove = this.getCombinations();
            if (gemsToRemove.size() == 0) {
                this.swap(src, dst);
            } else {
                do {
                    score += gemsToRemove.size();

                    for (Gem gem : gemsToRemove)
                        for (int j = gem.getLocation().getValue0(); j > 0; j--) {
                            this.swap(new Pair<>(j, gem.getLocation().getValue1()), new Pair<>(j - 1, gem.getLocation().getValue1()));
                        }

                    for (Gem j : gemsToRemove) {
                        Pair<Integer, Integer> loc = j.getLocation();
                        try {
                            this.board[loc.getValue0()][loc.getValue1()].setColor(this.isGemsFall ? Gem.Color.random() : null);
                            sideEffect.accept(this.board[loc.getValue0()][loc.getValue1()].getColor(), loc);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    if (!isSequence()) {
                        shuffle();
                        System.out.println("SHUFFLE");
                    }

                    gemsToRemove = this.getCombinations();

                    //printBoard();

                    System.out.println("END OF TURN");

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } while (gemsToRemove.size() != 0);
            }
        } else {
            System.out.println("Illegal Move");
        }

        return score;
    }

    public int turn(Gem a, Gem b) {
        return this.turn(a, b, new BiConsumer<Gem.Color, Pair<Integer, Integer>>() {
            @Override
            public void accept(Gem.Color color, Pair<Integer, Integer> objects) {

            }
        });
    }

    protected void shuffle() {
        LinkedList<Gem> gems = new LinkedList<>(Gem.getAllGems());
        Gem tmp, tmp2;
        Random rand = new Random();
        int rand_index, pivot = gems.size() - 1;

        while (pivot > 0) {
            System.out.println("PIVOT: " + pivot);
            rand_index = rand.nextInt(pivot + 1);
            pivot--;

            /*
            tmp = gems.get(rand_index);
            tmp2 = gems.get(pivot);

            gems.remove(rand_index);
            gems.remove(pivot);

            gems.add(rand_index - 1, tmp2);
            gems.add(pivot - 1, tmp);

             */

            Collections.swap(gems, rand_index, pivot);
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                /*rand_index = rand.nextInt(gems.size());
                swap(gems.get(rand_index).getLocation(), gems.getLast().getLocation());
                gems.remove(rand_index);*/
                swap(new Pair<>(i, j), gems.get(i * board[i].length + j).getLocation());
            }
        }
    }

    public void printBoard() {
        for (int i = 0; i < this.board.length; i++) {
            for (int j = 0; j < this.board[i].length; j++) {
                if (Gem.Color.WHITE.equals(board[i][j].getColor())) {
                    System.out.print("W ");
                } else if (Gem.Color.YELLOW.equals(board[i][j].getColor())) {
                    System.out.print("Y ");
                } else if (Gem.Color.RED.equals(board[i][j].getColor())) {
                    System.out.print("R ");
                } else if (Gem.Color.PURPLE.equals(board[i][j].getColor())) {
                    System.out.print("P ");
                } else if (Gem.Color.ORANGE.equals(board[i][j].getColor())) {
                    System.out.print("O ");
                } else if (Gem.Color.GREEN.equals(board[i][j].getColor())) {
                    System.out.print("G ");
                } else if (Gem.Color.BLUE.equals(board[i][j].getColor())) {
                    System.out.print("B ");
                }
            }
            System.out.println();
        }
    }

    /*/*public boolean play(Logic_Board.Player entry) {

        System.out.println("Round " + (state.get(entry) % 2 + 1));
        System.out.println(state.entrySet());

        if (state.get(entry) == 0) {
            System.out.println("IF1");
            state.put(entry, 2);
            if (this.isGameRoundOver())
                this.gameRoundsLeft--;
            return false;
        }

        if (this.graph.getSequenceGems(Graph.findBonus).size() != 0) {
            System.out.println("IF2");
            state.put(entry, state.get(entry) + 1);
        }

        System.out.println("State Before: " + state.get(entry));
        state.put(entry, state.get(entry) - 1);
        System.out.println("State After: " + state.get(entry));

        // System.out.println("Round " + state.get(entry));

        return state.get(entry) != 0;
        //};
    }*/

    /*
    protected boolean isGameRoundOver() {
        return state.get(Player.Computer) == 0 && state.get(Player.Human) == 0;
    }
    */
    protected boolean isGameOver() {
        return org.dzsh.Player.isGameRoundOver() && org.dzsh.Player.getGameRoundsLeft() == 0;
    }

    /*
    public Integer getPlayerScore() {
        return playerScore;
    }

    public void addPlayerScore(Integer addedScore) {
        this.playerScore += addedScore;
    }

    public Integer getComputerScore() {
        return computerScore;
    }

    public void addComputerScore(Integer addedScore) {
        this.computerScore += addedScore;
    }
    */

    public void setGemsFall(boolean gemsFall) {
        isGemsFall = gemsFall;
    }
}
