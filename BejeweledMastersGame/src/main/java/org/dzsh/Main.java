package org.dzsh;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
// import javafx.util.Pair;
import org.javatuples.Pair;

import java.util.concurrent.atomic.AtomicBoolean;

public class Main extends Application {

    Stage window;
    Scene scene;

    Board board;
    HBox roundLayout;
    HBox scoreLayout;
    GridPane boardLayout;
    BorderPane layout;

    Scoreboard scoreboard;
    TableView<UserScore> scoreTable;
    VBox scoreBoardLayout;

    VBox mainMenuLayout;

    public boolean exitConfirmGameBoard() {
        if (board.gameFinish()) {
            mainMenuScreen();
            return true;
        } else {
            return false;
        }
    }

    public void gameBoardScreen(String username) {
        roundLayout = new HBox();
        roundLayout.setPadding(new Insets(10, 10, 10, 10));
        roundLayout.setAlignment(Pos.CENTER);

        scoreLayout = new HBox();
        scoreLayout.setPadding(new Insets(10, 10, 10, 10));
        scoreLayout.setSpacing(220);

        boardLayout = new GridPane();
        boardLayout.setPadding(new Insets(10, 10, 10, 10));
        boardLayout.setVgap(8);
        boardLayout.setHgap(10);

        Object lock = new Object();

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                Runnable updater = new Runnable() {

                    @Override
                    public void run() {
                        board = new Board(board);
                    }
                };

                while (true) {
                    synchronized (lock) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    // UI update is run on the Application thread
                    Platform.runLater(updater);
                }
            }

        });

        // don't let thread prevent JVM shutdown
        thread.setDaemon(true);
        thread.start();

        layout = new BorderPane();
        layout.setTop(roundLayout);
        layout.setCenter(scoreLayout);
        layout.setBottom(boardLayout);

        window.setOnCloseRequest(e -> {
            e.consume();

            exitConfirmGameBoard();
        });

        scene = new Scene(layout, 350, 370);

        if (scoreboard == null) scoreboard = new Scoreboard(null); //scoreboard = Scoreboard.getInstance();

        board = new Board(roundLayout, scoreLayout, boardLayout, layout, scoreboard, username, window, scene , thread, lock, new Pair<>(7, 7));

        window.setScene(scene);
        window.show();

    }

    public boolean exitConfirmScoreboard() {
        Alert isExit = new Alert(Alert.AlertType.CONFIRMATION);
        //isExit.setTitle("Do You Want to Exit The Game?");
        isExit.setHeaderText("Do You Want to Exit The Score Board?\n(Will Return Back to Main Menu)");

        ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);

        isExit.getButtonTypes().setAll(yesButton, noButton);

        AtomicBoolean isExitFlag = new AtomicBoolean(false);

        isExit.showAndWait().ifPresent(type -> {
            if (type.getButtonData() == ButtonBar.ButtonData.YES) {
                isExitFlag.set(true);
                scoreboard.closeDB();
                mainMenuScreen();
            }
        });
        return isExitFlag.get();
    }

    public void scoreBoardScreen() {

        scoreTable = new TableView<>();

        TableColumn<UserScore, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setMinWidth(100);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("Id"));

        TableColumn<UserScore, String> usernameColumn = new TableColumn<>("Username");
        usernameColumn.setMinWidth(150);
        usernameColumn.setCellValueFactory(new PropertyValueFactory<>("Username"));

        TableColumn<UserScore, Integer> scoreColumn = new TableColumn<>("Score");
        scoreColumn.setMinWidth(100);
        scoreColumn.setCellValueFactory(new PropertyValueFactory<>("Score"));

        scoreTable.getColumns().addAll(idColumn, usernameColumn, scoreColumn);

        window.setOnCloseRequest(e -> {
            e.consume();
            exitConfirmScoreboard();
        });

        scoreBoardLayout = new VBox();
        scoreBoardLayout.getChildren().addAll(scoreTable);

        scene = new Scene(scoreBoardLayout, 350, 350);

        /*scoreboard = Scoreboard.getInstance();
        Scoreboard.getInstance().setScoreTable(scoreTable);
        */

        scoreboard = new Scoreboard(scoreTable);
        window.setScene(scene);
        window.show();

    }

    public boolean exitConfirmMainMenu() {
        Alert isExit = new Alert(Alert.AlertType.CONFIRMATION);
        //isExit.setTitle("Do You Want to Exit The Game?");
        isExit.setHeaderText("Do You Want to Exit The Game?");

        ButtonType yesButton = new ButtonType("Yes", ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType("No", ButtonBar.ButtonData.NO);

        isExit.getButtonTypes().setAll(yesButton, noButton);

        AtomicBoolean isExitFlag = new AtomicBoolean(false);

        isExit.showAndWait().ifPresent(type -> {
            if (type.getButtonData() == ButtonBar.ButtonData.YES) {
                isExitFlag.set(true);
                window.close();
            }
        });
        return isExitFlag.get();
    }

    public void mainMenuScreen() {
        mainMenuLayout = new VBox();
        mainMenuLayout.setAlignment(Pos.CENTER);
        mainMenuLayout.setPadding(new Insets(10, 10, 50, 10));
        mainMenuLayout.setSpacing(40);

        Label menuWelcomeTitle = new Label("Welcome to\n");
        menuWelcomeTitle.setFont(new Font(Font.getDefault().getName(), Font.getDefault().getSize()));

        Label menuGameTitle = new Label("Bejeweled Masters");
        menuGameTitle.setFont(new Font(Font.getDefault().getName(), Font.getDefault().getSize() * 2));

        Button gameBoardButton = new Button("Play (User vs. Computer)");
        gameBoardButton.setOnAction(e -> {
            TextInputDialog customUserName = new TextInputDialog();
            customUserName.setContentText("Username");
            customUserName.setHeaderText("Enter Your Username:\n(If left empty - Default Username will be used) ");
            customUserName.getDialogPane().getButtonTypes().remove(1);
            customUserName.showAndWait();

            gameBoardScreen(customUserName.getEditor().getText());
        });

        Button scoreBoardButton = new Button("Score Board");
        scoreBoardButton.setOnAction(e -> {
            scoreBoardScreen();
        });

        Button quitButton = new Button("Quit");
        quitButton.setOnAction(e -> {
            exitConfirmMainMenu();
        });

        window.setOnCloseRequest(e -> {
            e.consume();
            exitConfirmMainMenu();
        });

        mainMenuLayout.getChildren().addAll(menuWelcomeTitle, menuGameTitle, gameBoardButton, scoreBoardButton, quitButton);

        scene = new Scene(mainMenuLayout, 350, 360);

        window.setScene(scene);
        window.setResizable(false);
        window.show();
    }

    @Override
    public void start(javafx.stage.Stage primaryStage) throws Exception {

        window = primaryStage;
        window.setTitle("BejeweledMasters - Early Access");

        mainMenuScreen();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

