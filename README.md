### Bug Report #1

```
O R O G R W W 
O R G G R O P 
R O P G P G W 
P Y O O P G W 
O O W W Y O R 
R O R O O G P 
P W G P G R P 

3, 2 : 3
3, 3 : 3
3, 1 : 3
5, 1 : 3
4, 1 : 3
3, 1 : 3
2, 1 : 3
2, 2 : 3
2, 3 : 3
1, 3 : 3
2, 3 : 3
3, 3 : 3
O R Y Y R W W 
O W G O R O P 
R O O Y P G W 
P R P Y P G W 
O R W W Y O R 
R Y R O O G P 
P W G P G R P 
```

### Bug Report #2

```
Exception in thread "JavaFX Application Thread" java.lang.NullPointerException: Cannot invoke "org.dzsh.Gem.getColor()" because the return value of "org.dzsh.Gem.getGem(org.javatuples.Pair)" is null
	at org.dzsh.Board.isSequence(Board.java:210)
```

### TODO #1
* [x] DFS
* [x] Tree implementation
* [x] Forest DFS implementation
* [x] Adj function
* [x] Tree Print View
 
### TODO #2
* [x] בלי נפילה של אבנים מלמעלה - בלוגי => לטפל בשכנים (DFS)
* [x] isSequence (מנגנון של Shuffle) - להשתמש בשביל ליצור מוטציות
* [x] MinMax - לפי ניקוד מקסימלי
 
### TODO #3

1. [x] מוצא החלפות אפשריות
2. [x] מייצר את המוטציות (הלוחות)
3. [x] מחשב את התוצאה על סמך הרצפים שהצלחנו ליצור
* [x] לעשות את זה כמות פעמים