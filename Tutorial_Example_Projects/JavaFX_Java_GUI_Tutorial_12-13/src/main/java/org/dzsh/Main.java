package org.dzsh;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Main extends Application {

    Stage window;
    Scene scene;
    Button button;

    /*private void getChoice(ChoiceBox<String> choiceBox) {
        String choice = choiceBox.getValue();
        System.out.println(choice);
    }*/

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - Daniel Zigler");

        button = new Button("Click");

        ChoiceBox<String> choiceBox = new ChoiceBox<String>(); // new ChoiceBox<>();

        choiceBox.getItems().add("1");
        choiceBox.getItems().add("2");
        choiceBox.getItems().addAll("3", "4", "5");

        choiceBox.setValue("1"); // Default Value

        //button.setOnAction(e -> getChoice(choiceBox));
        choiceBox.getSelectionModel().selectedItemProperty().addListener( (v, oldValue, newValue) -> System.out.println(newValue) );

        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.getChildren().addAll(choiceBox, button);

        scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
