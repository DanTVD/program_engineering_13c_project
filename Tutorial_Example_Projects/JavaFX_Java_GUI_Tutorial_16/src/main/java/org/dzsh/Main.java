package org.dzsh;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.util.Stack;

public class Main extends Application {

    Stage window;
    TreeView<String> tree;

    public TreeItem<String> makeBranch(String title, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(true);
        parent.getChildren().add(item);
        return item;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("JavaFX - Daniel Zigler");

        TreeItem<String> root, branch1, branch2;

        root = new TreeItem<>();
        root.setExpanded(true);

        branch1 = makeBranch("1", root);
        makeBranch("1.1", branch1);
        makeBranch("1.2", branch1);
        makeBranch("1.3", branch1);

        branch2 = makeBranch("2", root);
        makeBranch("2.1", branch2);
        makeBranch("2.2", branch2);

        tree = new TreeView<>(root);
        tree.setShowRoot(false);
        tree.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            if(newValue != null)
                System.out.println(newValue.getValue());
        });

        StackPane layout = new StackPane();
        layout.getChildren().add(tree);
        Scene scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
