package org.dzsh;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
//import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application { // implements EventHandler<ActionEvent> {

    Button button;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Title of the Window");

        button = new Button("Click me");
        //button.setText("Click me");
        button.setOnAction(e -> {
            System.out.println("Lambda Expression was Called");
            System.out.println("Lambda Expression was Called 2");
        });
        /*new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Anon Inner Class was Called");
            }
        }*/

        StackPane layout = new StackPane();
        layout.getChildren().add(button);

        Scene scene = new Scene(layout,300, 250);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /*@Override
    public void handle(ActionEvent event) {
        if(event.getSource() == button){
            System.out.println("button Was Called");
        }
    }*/

    public static void main(String[] args) {
        launch(args);
    }

}
